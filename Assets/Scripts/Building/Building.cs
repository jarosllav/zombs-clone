﻿using UnityEngine;
using UnityEngine.UI;
using System;

[System.Serializable]
public enum BuildingType {
    NORMAL, BASE, MINE, CANON
}

public class Building : MonoBehaviour
{
    // Events
    public static event Action<Building> onBuild;
    public static event Action<Building> onDestroy;

    // Public fields
    public BuildingData Data;
    [NonSerialized] public BuildingSlot Slot;
    public BuildingType Type;

    // Private fields
    private int health;
    private Slider healthBar;
    
    protected float lastUpdate;

    // Unity methods
    private void Awake() {
        this.health = this.Data.Health;
        this.healthBar = gameObject.GetComponentInChildren<Slider>();
        if(this.healthBar) {
            this.healthBar.gameObject.SetActive(false);
            this.healthBar.maxValue = health;
            this.healthBar.minValue = 0;
        }
    }

    private void OnEnable() {
        onBuild?.Invoke(this);
    }

    private void OnDestroy() {
        onDestroy?.Invoke(this);

        if(Slot && Slot.Used != 0) 
            Slot.SetUsed(Slot.Used - 1);
    }

    // Public methods

    /// <summary>
    /// Change building health value (and update healthBar)
    /// </summary>
    /// <param name="value">New health value</param>
    public void SetHealth(int value) {
        this.health = value;
        if(this.healthBar) 
            this.healthBar.value = value;
    }

    /// <summary>
    /// Change building health to max value (and hide healthBar)
    /// </summary>
    public void FullHealth() {
        this.SetHealth(this.Data.Health);
        if(this.healthBar && this.healthBar.gameObject.activeSelf)
            this.healthBar.gameObject.SetActive(false);
    }

    /// <summary>
    /// Deal damage to building (and destroy GameObject when health is below 0)
    /// </summary>
    /// <param name="strength">Strength of hit</param>
    public void Hit(int strength = 1) {
        this.SetHealth(this.health - strength);

        if(this.healthBar && !this.healthBar.gameObject.activeSelf)
            this.healthBar.gameObject.SetActive(true);

        if(this.health <= 0) {
            Destroy(gameObject);
        }
    }
}