﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BuildingMine : Building
{
    public static event Action<BuildingMine> onClick;

    // Public fields
    [NonSerialized] public int CollectedGold = 0;
    public float CollectTime = 10f;
    public int CollectAmount = 1;

    // Private fields
    private TMPro.TextMeshProUGUI textGold;

    // Unit methods
    private void Start() {
        lastUpdate = CollectTime;
        textGold = GetComponentInChildren<TMPro.TextMeshProUGUI>();
        if(textGold)
            textGold.gameObject.SetActive(false); 
    }

    private void Update() {
        if(lastUpdate <= 0) {
            CollectedGold += CollectAmount;
            lastUpdate = CollectTime;

            if(textGold) {
                textGold.text = CollectedGold.ToString();
                if(CollectedGold > 0)
                    textGold.gameObject.SetActive(true);
            }
        }
        else {
            lastUpdate -= Time.deltaTime;
        }
    }

    private void OnMouseOver() {
        if(Input.GetMouseButtonDown(1)) {
            if(CollectedGold > 0) {
                onClick?.Invoke(this);
            }
        }
    }

    // Public methods
    public int DrawGold() {
        int gold = CollectedGold;
        CollectedGold = 0;
        if(textGold) 
            textGold.gameObject.SetActive(false);
        return gold;
    }
}
