﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingCanon : Building
{
    // Private fields
    private GameObject targetAttack;

    // Unity methods
    private void Start() {
        lastUpdate = Data.AttackSpeed;    
    }

    private void Update() {
        if(lastUpdate <= 0) {
            if(!GameManager.Instance.GetZombieSpawner().IsWaveStarted()) {
                return;
            }

            if(targetAttack == null) {
                Zombie[] enemies = FindObjectsOfType<Zombie>();
                GameObject target = null;
                float minDistance = Mathf.Infinity;

                foreach(Zombie enemy in enemies) {
                    float distance = Vector2.Distance(transform.position, enemy.transform.position);
                    if(distance <= Data.Radius && distance <= minDistance) {
                        minDistance = distance;
                        target = enemy.gameObject;
                    }
                }

                targetAttack = target;
            }

            if(targetAttack != null) {
                Vector2 direction = (targetAttack.transform.position - transform.position).normalized;
                GameObject bulletObject = Instantiate(Data.Ammunition, transform.position, Quaternion.identity);
                Bullet bullet = bulletObject.GetComponent<Bullet>();

                bullet.Velocity = direction * 15f;
                bullet.Origin = transform.position;
                bullet.Radius = Data.Radius;
                bullet.Damage = Data.Damage;
                lastUpdate = Data.AttackSpeed;
            }
        }
        else {
            lastUpdate -= Time.deltaTime;
        }
    }

    // Public methods

    
    // Private methods


}
