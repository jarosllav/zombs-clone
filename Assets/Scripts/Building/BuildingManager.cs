﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Tilemaps;
using UnityEngine.EventSystems;

public class BuildingManager : MonoBehaviour
{
    // Private fields
    private World world;
    private Player player;

    private List<Building> buildings = new List<Building>();
    private Building baseBuilding;
    private BuildingSlot selected;
    private GameObject preview;

    // Unity methods
    private void Awake() {
        world = FindObjectOfType<World>();
        player = FindObjectOfType<Player>();

        Building.onBuild += onBuild;
        Building.onDestroy += onDestroy;
    }

    private void Update() {
        if(this.preview && this.selected && this.preview.activeSelf)
            this.updatePreviewObject();
        
        this.handleInput();
    }

    // Private methods
    
    /// <summary>
    /// Create an object to preview selected building in world space
    /// </summary>
    private void createPreview() {
        this.preview = new GameObject("BuildingPreview");
        this.preview.transform.parent = GameObject.Find("_Dynamic").transform;
        SpriteRenderer renderer = this.preview.AddComponent<SpriteRenderer>();
        renderer.sortingLayerName = "Foreground";
    }

    /// <summary>
    /// Updates preview object, changes color depending on whther it can be placed.
    /// If player click Fire key - place bulding and removes building resources from player inventory.
    /// </summary>
    private void updatePreviewObject() {
        Vector3 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2Int objectSize = this.selected.Prefab.GetComponent<Building>().Data.Size;
        Vector3 position = new Vector3(Mathf.Floor(mouse.x), Mathf.Floor(mouse.y), 0);
        
        // Set preview object position
        this.preview.transform.position = position + new Vector3((objectSize.x / 2f), (objectSize.y / 2f), 0);

        if(this.baseBuilding == null && selected.Prefab.GetComponent<Building>().Type != BuildingType.BASE) {
            this.preview.GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.0f, 0.0f, 0.6f);
            return;
        }

        if(this.world.CanPlaceAtPosition(position, objectSize)) {
            BuildingData data = this.selected.Prefab.GetComponent<Building>().Data;
            if(data.Limit == 0 || this.selected.Used < data.Limit) {
                PlayerInventory inventory = this.player.GetInventory();
                if(inventory.GetItem(ResourcesTypes.Wood) >= data.Wood && inventory.GetItem(ResourcesTypes.Rock) >= data.Rock) {
                    Vector3 basePosition = this.baseBuilding ? this.baseBuilding.transform.position : this.player.transform.position;
                    float distance = Vector3.Distance(basePosition, new Vector3(position.x, position.y, 0));
                    if(distance < 6) { //todo: max distance based on base upgrade
                        this.preview.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
                        if(Input.GetButtonDown("Fire1") && !EventSystem.current.IsPointerOverGameObject()) {
                            this.placeBuilding(position, objectSize);

                            this.selected.Used++;
                            if(data.Limit > 0 && this.selected.Used >= data.Limit) {
                                this.preview.SetActive(false);
                                this.selected.GetComponent<Button>().interactable = false;
                            }

                            inventory.RemoveItem(ResourcesTypes.Wood, data.Wood);
                            inventory.RemoveItem(ResourcesTypes.Rock, data.Rock);
                        }
                        return;
                    }
                }
            }
        }
        
        this.preview.GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.0f, 0.0f, 0.6f);
    }

    /// <summary>
    /// Instantiate selected building object and call World to place it at given position
    /// </summary>
    /// <param name="position">Position of the object in the world (center of the object)</param>
    /// <param name="size">Size of the object expressed in world cells</param>
    private void placeBuilding(Vector3 position, Vector2Int size) {
        // Little fix for object position
        GameObject buildingObject = Instantiate(this.selected.Prefab, position + new Vector3(size.x / 2f, size.y / 2f, 0), Quaternion.identity);
        buildingObject.GetComponent<Building>().Slot = this.selected;
        world.Place(buildingObject, position, size);
    }

    /// <summary>
    /// Event handler for build new structure, looking for building base type.
    /// </summary>
    private void onBuild(Building building) {
        this.buildings.Add(building);

        if(building.Type == BuildingType.BASE) {
            this.baseBuilding = building;
        }
    }

    /// <summary>
    /// Event handler for destorying building, call world to erase building space
    /// </summary>
    private void onDestroy(Building building) {
        this.world.SetArea(false, building.transform.position, building.Data.Size);
        this.buildings.Remove(building);
    }

    /// <summary>
    /// Handle user input
    /// (hides preview object by clicking escape key)
    /// </summary>
    private void handleInput() {
        if(Input.GetKeyDown(KeyCode.Escape)) {
            this.preview.SetActive(false);
        }
    }

    // Public methods
    
    /// <summary>
    /// Choosing building from building menu
    /// </summary>
    /// <param name="slot">UI slot</param>
    public void ChooseSlot(BuildingSlot slot) {
        if(this.preview == null)
            this.createPreview();

        this.selected = slot;
        this.preview.GetComponent<SpriteRenderer>().sprite = this.selected.Prefab.GetComponent<Building>().Data.Texture;

        if(!this.preview.activeSelf)
            this.preview.SetActive(true);
    }

    /// <summary>
    /// Regenerates health of all instantiated buildings
    /// </summary>
    public void RegenerateBuildings() {
        foreach(Building obj in this.buildings) {
            obj.FullHealth();
        }
    }

    #if UNITY_EDITOR
    private void OnDrawGizmos() {
        if(selected != null && selected.Prefab != null) {
            Vector3 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            Gizmos.DrawSphere(mouse, 0.1f);

            Vector2Int objectSize = selected.Prefab.GetComponent<Building>().Data.Size;
            Vector3 position = new Vector3(Mathf.Floor(mouse.x), Mathf.Floor(mouse.y), 0);
        
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(position, 0.15f);

            for(int x = 0; x < objectSize.x; ++x) {
                for(int y = 0; y < objectSize.y; ++y) {
                    int cellX = (int)(position.x) + x;
                    int cellY = (int)(position.y) + y;

                    Gizmos.color = new Color(255, 0, 0, 1);
                    drawSquare(new Vector3(cellX, cellY, 0), new Vector3(1, 1, 0));
                }
            }

            Gizmos.color = new Color(0, 0, 255, 1);
            drawSquare(position, new Vector3(objectSize.x, objectSize.y, 0));

            Gizmos.color = Color.white;
        }
    }

    private void drawSquare(Vector3 position, Vector3 size) {
        Gizmos.DrawLine(position, position + new Vector3(size.x, 0, 0));
        Gizmos.DrawLine(position + new Vector3(size.x, 0, 0), position + new Vector3(size.x, size.y, 0));
        Gizmos.DrawLine(position + new Vector3(size.x, size.y, 0), position + new Vector3(0, size.y, 0));
        Gizmos.DrawLine(position + new Vector3(0, size.y, 0), position);
    }
    #endif
}
