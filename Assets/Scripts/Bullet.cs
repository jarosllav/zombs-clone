﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // Public fields
    [System.NonSerialized] public int Damage;
    [System.NonSerialized] public float Radius;
    [System.NonSerialized] public Vector3 Origin;
    [System.NonSerialized] public Vector2 Velocity;

    // Unity methods
    private void FixedUpdate() {
        if(Vector2.Distance(Origin, transform.position) > Radius) {
            Destroy(gameObject);
            return;
        }

        Collider2D collider = Physics2D.OverlapCircle(transform.position, 0.16f);
        if(collider) {
            if(collider.tag == "Zombie") { 
                collider.gameObject.GetComponent<Zombie>().Hit(Damage);
                Destroy(gameObject);
                return;
            }
        }

        transform.Translate(Velocity * Time.fixedDeltaTime);
    }
}
