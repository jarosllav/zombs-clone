using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CellularAutomata : MonoBehaviour
{
    /*public Vector2 Size = new Vector2(50, 50);
    public TileBase tile01;
    public TileBase tile02;

    private Tilemap[] layers;
    private Tile[] map;
    private bool[,] cellMap;
    private Vector2 mapSize;

    [SerializeField] private int deathLimit = 4;
    [SerializeField] private int birthLimit = 5;
    [SerializeField] private int simulationSteps = 10;
    [SerializeField] private float chanceToStartAlive = 0.45f;

    [SerializeField] private float radius = 1.5f;
    [SerializeField] private int limit = 10;

    public GameObject PrefabTree;
    public GameObject PrefabRock;

    private PoissonDistribution poissonDist = new PoissonDistribution();

    public bool[,] GetMap() { 
        return cellMap;
    }

    public void Generate() {
        loadLayers();

        mapSize = new Vector2(Size.x + 2, Size.y + 2);

        for(int i = 0; i < layers.Length; ++i) {
            layers[i].size = new Vector3Int((int)mapSize.x, (int)mapSize.y, 0);
        }
        
        // Generate map
        Tilemap tilemap = layers[(int)TilemapLayers.Base];
        Tilemap collider = layers[(int)TilemapLayers.Collider];

        tilemap.ClearAllTiles();
        collider.ClearAllTiles();

        cellMap = new bool[(int)mapSize.x, (int)mapSize.y];

        for(int y = 0; y < mapSize.y; ++y) {
            for(int x = 0; x < mapSize.x; ++x) {
                bool isAlive = Random.Range(0f, 1f) < chanceToStartAlive;
                cellMap[x, y] = isAlive;
            }
        }       

        for(int i = 0; i < simulationSteps; ++i) {
            cellMap = processSimulation();
        }

        for(int y = 0; y < mapSize.y; ++y) {
            for(int x = 0; x < mapSize.x; ++x) {
                if(y == 0 || y == mapSize.y - 1 || x == 0 || x == mapSize.x - 1 ||
                    y == 1 || y == mapSize.y - 2 || x == 1 || x == mapSize.x - 2)
                    cellMap[x, y] = true;

                tilemap.SetTile(new Vector3Int(x, y, 0), tile01);

                if(cellMap[x, y]) {
                    collider.SetTile(new Vector3Int(x, y, 0), tile02);
                }
            }
        } 

        if(Application.isPlaying)
            placeResources();

        transform.position = -1f * (Vector3)(mapSize / 2f);
    }

    private void placeResources() {
        if(PrefabTree) {
            poissonDist.Limit = limit;
            poissonDist.Radius = radius;
            poissonDist.Size = mapSize;

            Vector3[] points = poissonDist.Generate();
            Transform treesHolder = GameObject.Find("_Dynamic").transform;

            for(int i = 0; i < points.Length; ++i) {
                int x = Mathf.FloorToInt(points[i].x);
                int y = Mathf.FloorToInt(points[i].y);
                if(cellMap[x, y] || cellMap[x - 1, y] || cellMap[x, y - 1] || cellMap[x - 1, y - 1])
                    continue;

                Vector3 position = new Vector3(x, y, 0) - (Vector3)(mapSize / 2f);
                GameObject tree = Instantiate(PrefabTree, position, Quaternion.identity);
                tree.transform.parent = treesHolder;

                cellMap[x, y] = true;
                cellMap[x + 1, y] = true;
                cellMap[x, y + 1] = true;
                cellMap[x + 1, y + 1] = true;
            }

            points = poissonDist.Generate();

            for(int i = 0; i < points.Length; ++i) {
                int x = Mathf.FloorToInt(points[i].x);
                int y = Mathf.FloorToInt(points[i].y);
                if(cellMap[x, y] || cellMap[x - 1, y] || cellMap[x, y - 1] || cellMap[x - 1, y - 1])
                    continue;

                Vector3 position = new Vector3(x, y, 0) - (Vector3)(mapSize / 2f);
                GameObject rock = Instantiate(PrefabRock, position, Quaternion.identity);
                rock.transform.parent = treesHolder;

                cellMap[x, y] = true;
                cellMap[x + 1, y] = true;
                cellMap[x, y + 1] = true;
                cellMap[x + 1, y + 1] = true;
            }
        }
    }

    private bool[,] processSimulation() {
        bool[,] map = new bool[(int)mapSize.x, (int)mapSize.y];

        for(int y = 0; y < mapSize.y; ++y) {
            for(int x = 0; x < mapSize.x; ++x) {
                int neigbours = countNeighbours(x, y);
                if(cellMap[x,y]) {
                    map[x, y] = neigbours >= deathLimit;      
                }
                else {
                    map[x, y] = neigbours > birthLimit;   
                }
            }
        }  

        return map;
    }

    private int countNeighbours(int x, int y) {
        int count = 0;
        for(int i = -1; i < 2; i++) {
            for(int j = -1; j < 2; j++) {
                int neighbourX = x + i;
                int neighbourY = y + j;

                if(i == 0 && j == 0)
                    continue;
                else if(neighbourX < 0 || neighbourY < 0 || neighbourX >= mapSize.x || neighbourY >= mapSize.y) {
                    ++count;
                }
                else if(cellMap[neighbourX, neighbourY]) {
                    ++count;
                }
            }
        }
        return count;
    }

    private int closestNeighbours(int x, int y) {
        int count = 0;
        if(x > 0 && cellMap[x - 1, y])
            ++count;
        if(x < mapSize.x - 1 && cellMap[x + 1, y])
            ++count;
        if(y > 0 && cellMap[x, y - 1])
            ++count;
        if(y < mapSize.y - 1 && cellMap[x, y + 1])
            ++count;

        return count;
    }

    private void loadLayers() {
        layers = GetComponentsInChildren<Tilemap>();
        if(layers.Length <= 0)
            Debug.Log("Doesnt exists any tilemap layer");
    }*/
}
