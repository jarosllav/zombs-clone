﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor
{
    public override void OnInspectorGUI() {
        GameManager manager = (GameManager)target;

        EditorGUILayout.LabelField("gameMan settings...");
        DrawDefaultInspector();
    }
}
#endif