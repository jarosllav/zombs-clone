﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(World))]
public class WorldGeneratorEditor : Editor
{
    public override void OnInspectorGUI() {
        World world = (World)target;

        DrawDefaultInspector();
       
        if(GUILayout.Button("Generate")) {
            world.Generate();
        }
    }
}
#endif