﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipmentSlot : MonoBehaviour
{
    public EquipmentType Type;
    public EquipmentData Data;

    public void SetData(EquipmentData data) {
        Data = data;
        Image image = gameObject.transform.GetChild(0).GetComponentInChildren<Image>();
        image.sprite = Data.Texture;

        Button button = GetComponent<Button>();
        if(button && !button.interactable) {
            button.interactable = true;
        }
    }
}
