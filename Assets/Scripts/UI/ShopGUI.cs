﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopGUI : MonoBehaviour
{
    // Public fields
    public TabGroup Group;

    // Private fields
    private bool isOpen = false;

    // Unity methods
    private void Awake() {
        this.Hide();
    }
    
    // Public methods
    public void Show() {
        for(int i = 0; i < transform.childCount; ++i) {
            transform.GetChild(i).gameObject.SetActive(true);
        }
        this.isOpen = true;
    }

    public void Hide() {
        for(int i = 0; i < transform.childCount; ++i) {
            transform.GetChild(i).gameObject.SetActive(false);
        }
        this.isOpen = false;
    }

    public void Toggle() {
        if(this.isOpen)
            this.Hide();
        else
            this.Show();
    }

    public void Buy() {
        if(Group.SelectedItem != null) {
            ShopItem item = Group.SelectedItem.GetComponent<ShopItem>();
            PlayerInventory inventory = GameManager.Instance.GetPlayer().GetComponent<PlayerInventory>();
            int playerGold = inventory.GetItem(ResourcesTypes.Gold);

            if(playerGold >= item.Cost) {
                inventory.RemoveItem(ResourcesTypes.Gold, item.Cost);                
                int slotId = Group.SelectedTab.transform.GetSiblingIndex();
                inventory.Equip(slotId, item.Data);
                
                if(inventory.GetCurrentSlotId() != slotId) {
                    inventory.ChooseSlot(slotId);
                }

                GameObject items = Group.ObjectsToSwap[slotId];
                for(int i = 0; i < items.transform.childCount; i++) {
                    Transform child = items.transform.GetChild(i);
                    if(child.GetComponent<ShopItem>().Data.Quality <= item.Data.Quality) {
                        child.GetComponent<Button>().interactable = false;
                    }
                    else {
                        break;
                    }
                }
                Group.SelectedItem = null;
            }
            else print("You dont have enough gold for this item");
        }
    }
}
