﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourcesGUI : MonoBehaviour
{
    // Private fields
    [SerializeField] private TMPro.TextMeshProUGUI woodUI; 
    [SerializeField] private TMPro.TextMeshProUGUI rockUI; 
    [SerializeField] private TMPro.TextMeshProUGUI goldUI; 

    // Unity methods
    private void Awake() {
        PlayerInventory.onChangeItem += onChangeItem;
    } 

    // Private methods
    private void onChangeItem(int resourceType, int amount) {
        if(resourceType == 0)
            woodUI.text = amount.ToString();
        else if(resourceType == 1)
            rockUI.text = amount.ToString();
        else if(resourceType == 2)
            goldUI.text = amount.ToString(); 
    }
}
