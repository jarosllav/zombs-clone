﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverGUI : MonoBehaviour
{
    // Public fields
    public TMPro.TextMeshProUGUI Info;

    // Private fields
    private bool isOpen = false;

    // Unity methods
    private void Awake() {
        this.Hide();
    }

    // Public methods
    public void SetText(string text) {
        this.Info.SetText(text);
    }
    
    public void Show() {
        for(int i = 0; i < transform.childCount; ++i) {
            transform.GetChild(i).gameObject.SetActive(true);
        }
        this.isOpen = true;
    }

    public void Hide() {
        for(int i = 0; i < transform.childCount; ++i) {
            transform.GetChild(i).gameObject.SetActive(false);
        }
        this.isOpen = false;
    }

    public void Toggle() {
        if(this.isOpen)
            this.Hide();
        else
            this.Show();
    }
}