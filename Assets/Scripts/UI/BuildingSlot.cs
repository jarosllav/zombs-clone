using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BuildingSlot : MonoBehaviour, IPointerEnterHandler
{
    // Public fields
    [System.NonSerialized] public int Used = 0;
    [System.NonSerialized] public int MaxUse;
    public GameObject Prefab;

    // Unity methods
    private void Awake() {
        if(Prefab != null) {
            Image image = transform.GetChild(0).GetComponent<Image>();
            Building building = Prefab.GetComponent<Building>();
            image.sprite = building.Data.Texture;
            MaxUse = building.Data.Limit;

            Button button = GetComponent<Button>();
            button.onClick.AddListener(this.onClick);
        }    
        else {
            GetComponent<Button>().interactable = false;
        }
    }

    // Public methods
    public void OnPointerEnter(PointerEventData pointerEventData) {
        if(Prefab) {
            Tooltip tooltip = FindObjectOfType<Tooltip>();
            if(tooltip) {
                tooltip.SetData(Prefab.GetComponent<Building>().Data);
                tooltip.Show(transform.position);
            }
        }
    }

    public void SetUsed(int value) {
        Used = value;
        if(!GetComponent<Button>().interactable && Used < MaxUse)
            GetComponent<Button>().interactable = true;
    }

    // Private methods
    private void onClick() {
        GameManager.Instance.GetBuildingManager().ChooseSlot(this);
    }
}
