﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour
{
    // Private fields
    [SerializeField] private TMPro.TextMeshProUGUI header; 
    [SerializeField] private TMPro.TextMeshProUGUI wood; 
    [SerializeField] private TMPro.TextMeshProUGUI rock; 
    [SerializeField] private TMPro.TextMeshProUGUI limit; 

    // Unity methods
    private void Awake() {
        transform.GetChild(0).gameObject.SetActive(false);      
    }

    private void Update() {
        
    }

    // Public methods
    public void SetData(BuildingData data) {
        header.text = data.Name;
        wood.text = "Wood: " + data.Wood;
        rock.text = "Rock: " + data.Rock;
        limit.text = "Limit: " + (data.Limit == 0 ? "unlimited" : data.Limit.ToString());
    }

    public void Show(Vector3 position) {
        position.x -= GetComponent<RectTransform>().rect.width;
        transform.position = position;
        transform.GetChild(0).gameObject.SetActive(true);       
    }

    public void Hide() {
        transform.GetChild(0).gameObject.SetActive(false); 
    }
}
