﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabGroup : MonoBehaviour
{
    [System.NonSerialized] public List<TabButton> Buttons;
    [System.NonSerialized] public TabButton SelectedTab;
    [System.NonSerialized] public GameObject SelectedItem;

    public List<GameObject> ObjectsToSwap;

    public void Subscribe(TabButton button) {
        if(Buttons == null) {
            Buttons = new List<TabButton>();
        }
        Buttons.Add(button);
    }

    public void OnTabEnter(TabButton button) {
        resetTabs();
    }
    public void OnTabExit(TabButton button) {
        resetTabs();
    }
    public void OnTabSelected(TabButton button) {
        resetTabs();

        int id = button.transform.GetSiblingIndex();
        for(int i = 0; i < ObjectsToSwap.Count; ++i) {
            if(i == id) {
                ObjectsToSwap[i].SetActive(true);
            }
            else {
                ObjectsToSwap[i].SetActive(false);
            }
        }

        SelectedTab = button;
        SelectedItem = null;
    }

    public void OnItemSelected(int id) {
        if(SelectedTab == null)
            return;

        SelectedItem = ObjectsToSwap[SelectedTab.transform.GetSiblingIndex()].transform.GetChild(id).gameObject;
        if(SelectedItem != null) {

        }
    }

    private void resetTabs() {
        foreach(TabButton button in Buttons) {
            // ..
        }
    }
}
