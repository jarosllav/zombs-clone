﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BuildingGUI : MonoBehaviour, IPointerExitHandler
{
    // Private fields
    private bool isOpen = false;

    // Unity methods
    private void Awake() {
        //this.Hide();
    }

    // Public methods
    public void Show() {
        for(int i = 0; i < transform.childCount; ++i) {
            transform.GetChild(i).gameObject.SetActive(true);
        }
        this.isOpen = true;
    }

    public void Hide() {
        for(int i = 0; i < transform.childCount; ++i) {
            transform.GetChild(i).gameObject.SetActive(false);
        }
        this.isOpen = false;
    }

    public void Toggle() {
        if(this.isOpen)
            this.Hide();
        else
            this.Show();
    }

    public void OnPointerExit(PointerEventData eventData) {
        Tooltip tooltip = FindObjectOfType<Tooltip>();
        if(tooltip) {
            tooltip.Hide();
        }
    }
}
