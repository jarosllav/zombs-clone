﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[ExecuteInEditMode]
public class GameTimer : MonoBehaviour
{
    public float Modifier = 1f;
    public bool AffectUpdate = false;
    public TMPro.TextMeshProUGUI TextUI;
    public UnityEvent<int, int> Event;
    
    private int minutes;
    private int seconds;
    private float lastTick;

    private void Update() {
        if(Time.time - lastTick > 1f) {
            ++seconds;
            if(seconds > 59) {
                seconds = 0;
                ++minutes;
                if(minutes > 59) {
                    minutes = 0;
                }
            }
            lastTick = Time.time;

            TextUI.SetText("Time: " + string.Format("{0:00}:{1:00}", minutes, seconds));
            Event.Invoke(minutes, seconds);
        }
    }

    private void OnValidate() {
        if(AffectUpdate)
            Time.timeScale = Modifier;
    }
}

