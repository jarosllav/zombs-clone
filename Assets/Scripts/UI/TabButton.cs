﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Image))]
public class TabButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public TabGroup Group;
    public Image Background;

    private void Awake() {
        Background = GetComponent<Image>();
        Group.Subscribe(this);
    }

    public void OnPointerClick(PointerEventData eventData) {
        Group.OnTabSelected(this);
    }

    public void OnPointerExit(PointerEventData eventData) {
        Group.OnTabExit(this);
    }

    public void OnPointerEnter(PointerEventData eventData) {
        Group.OnTabEnter(this);
    }
}
