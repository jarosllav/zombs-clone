﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerInventory : MonoBehaviour
{
    // Events
    public static event Action<int> onChooseSlot;       // <slotId>
    public static event Action<int, int> onChangeItem;  // <resourceType, amount>

    // Private fields
    [SerializeField] private EquipmentData[] eqDatas;
    [SerializeField] private GameObject[] eqSlotsUI;
    [SerializeField] private GameObject eqHold;
    private EquipmentSlot[] eqSlots;
    private int equipped = 0;

    private Dictionary<ResourcesTypes, int> items;

    // Unity methods
    private void Start() {
        this.items = new Dictionary<ResourcesTypes, int>();
        this.eqSlots = new EquipmentSlot[eqSlotsUI.Length];
        for(int i = 0; i < eqSlotsUI.Length; ++i) {
            eqSlots[i] = eqSlotsUI[i].GetComponent<EquipmentSlot>();
            if(eqSlots[i].Type == EquipmentType.HARVEST) {
                EquipmentData data = eqDatas[0];
                eqSlots[i].SetData(data);
            }
        }

        BuildingMine.onClick += onClickMine;

        #if UNITY_EDITOR
            AddItem(ResourcesTypes.Wood, 115);
            AddItem(ResourcesTypes.Rock, 100);
            AddItem(ResourcesTypes.Gold, 1000);
        #endif
    }

    private void OnDestroy() {
        onChooseSlot = null;
        onChangeItem = null;
    }

    // Public methods

    /// <summary>
    /// Adds a given quantity of resource type into items container
    /// </summary>
    /// <param name="resourceType">ResourcesTypes type of item</param>
    /// <param name="amount">Amount to add</param>
    public void AddItem(ResourcesTypes resourceType, int amount = 1) {
        if(this.items.ContainsKey(resourceType))
            this.items[resourceType] += amount;
        else 
            this.items.Add(resourceType, amount);

        onChangeItem?.Invoke((int)resourceType, this.items[resourceType]);
    }

    /// <summary>
    /// Removes a given quantity of resource type from items container
    /// </summary>
    /// <param name="resourceType">ResourcesTypes type of item</param>
    /// <param name="amount">Amount to add</param>
    public void RemoveItem(ResourcesTypes resourceType, int amount = 1) {
        if(this.items.ContainsKey(resourceType)) {
            this.items[resourceType] -= amount;
            if(this.items[resourceType] < 0) {
                this.items.Remove(resourceType);
            }

            onChangeItem?.Invoke((int)resourceType, this.items.ContainsKey(resourceType) ? this.items[resourceType] : 0);
        }
    }

    /// <summary>
    /// Equip a new tool to specific quickbar slot
    /// </summary>
    /// <param name="slotId">Quickbar slot ID</param>
    /// <param name="data">EquipmentData reference to new tool data</param>
    public void Equip(int slotId, EquipmentData data) {
        if(this.eqSlots[slotId] != null) {
            this.eqSlots[slotId].SetData(data);

            SpriteRenderer renderer = this.eqHold.GetComponent<SpriteRenderer>();
            renderer.sprite = this.eqSlots[slotId].Data.Texture;
        }
    }
    
    /// <summary>
    /// Choose a tool from specific quickbar slot (if its not null)
    /// </summary>
    /// <param name="slotId">Quickbar slot ID</param>
    public void ChooseSlot(int slotId) {
        if(eqHold) {
            if(eqSlots[slotId].Data != null) {
                SpriteRenderer renderer = eqHold.GetComponent<SpriteRenderer>();
                renderer.sprite = eqSlots[slotId].Data.Texture;
                equipped = slotId;

                onChooseSlot?.Invoke(slotId);
            }
        }
    }

    // Private methods

    /// <summary>
    /// Event handler of clicked BuildingMine, draw collected gold from building and give to player
    /// </summary>
    /// <param name="mine">Reference of BuildingMine instance clicked</param>
    private void onClickMine(BuildingMine mine) {
        int goldDraw = mine.DrawGold();
        this.AddItem(ResourcesTypes.Gold, goldDraw);
    }

    // Public methods
    public EquipmentSlot GetEquipmentSlot(int slotId) { return this.eqSlots[slotId]; }
    public EquipmentSlot GetCurrentSlot() { return this.eqSlots[equipped]; }
    public int GetItem(ResourcesTypes res) { return this.items[res]; }
    public int GetCurrentSlotId() { return this.equipped; }
}
