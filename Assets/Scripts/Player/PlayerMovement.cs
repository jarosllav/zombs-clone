﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    // Private fields
    [SerializeField] private float speed;
    private Vector2 velocity;
    private Rigidbody2D rigidbody;

    // Unity methods
    private void Awake() {
        this.rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update() {
        if(!GameManager.Instance.IsGamePasued()) {
            this.handleInput();
            this.rotateToMouse();
        }
    }

    // Private methods

    /// <summary>
    /// Handle player movement input
    /// </summary>
    private void handleInput() {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        this.velocity.x = horizontal * speed;
        this.velocity.y = vertical * speed;
    }

    /// <summary>
    /// Rotate player game object to mouse position
    /// </summary>
    private void rotateToMouse() {
        Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        position.Normalize();
        float angle = Mathf.Rad2Deg * Mathf.Atan2(position.y, position.x);
        transform.rotation = Quaternion.Euler(0f, 0f, angle - 90);
    }

    // Public methods
    public void Move() {
        rigidbody.velocity = this.velocity;
    }
}
