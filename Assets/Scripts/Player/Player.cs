using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerMovement)), RequireComponent(typeof(PlayerInventory))]
public class Player : MonoBehaviour
{
    // Private fields
    private PlayerMovement movement;
    private PlayerInventory inventory;
    private Animator animator;

    // Unity methods
    void Start() {
        movement = GetComponent<PlayerMovement>();
        inventory = GetComponent<PlayerInventory>();
        animator = GetComponent<Animator>();

        PlayerInventory.onChooseSlot += onChooseSlot;
    }

    private void OnDestroy() {
        movement = null;
        inventory = null;
        animator = null;
    }

    void Update() {
        movement.Move();

        this.handleInput();
    }

    // Private methods
    private void Anim_StopFire() {}

    /// <summary>
    /// Animation event for tools, checks if player hits something and handle this
    /// </summary>
    private void Anim_Hit() {
        gameObject.layer = 2;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.up, 2f);
        if(hit) {
            string tag = hit.collider.tag;
            EquipmentSlot slot = inventory.GetCurrentSlot();
            if(tag == "Resource") {
                if(slot.Type == EquipmentType.HARVEST) {
                    Resource resource = hit.collider.gameObject.GetComponent<Resource>();
                    resource.Shake();

                    HarvestData data = (HarvestData)slot.Data;
                    inventory.AddItem(resource.Type, data.CollectSize);
                }
            }
            else if(tag == "Zombie") {
                Zombie zombie = hit.collider.gameObject.GetComponent<Zombie>();

                if(slot.Type == EquipmentType.MELEE) {
                    MeleeData data = (MeleeData)slot.Data;
                    zombie.Hit(data.Damage);
                }
                else {
                    zombie.Hit(1);
                }
            }
        }
        gameObject.layer = 0;
    }

    /// <summary>
    /// Event handler for choosing tool slot change animator value    
    /// </summary>
    private void onChooseSlot(int slotId) {
        animator.SetInteger("type", (int)inventory.GetCurrentSlot().Type);
    }

    /// <summary>
    /// Handle player input (like quickslots, menus)
    /// </summary>
    private void handleInput() {
        // Default: KeyCode.R
        if(Input.GetButtonDown("ShopKey")) {  
            FindObjectOfType<ShopGUI>().Toggle();
        }

        // Default: KeyCode.T
        if(Input.GetButtonDown("BuildingKey")) {
            FindObjectOfType<BuildingGUI>().Toggle();
        }

        if(Input.GetKeyDown(KeyCode.Escape)) {
            if(GameManager.Instance.IsGamePasued())
                GameManager.Instance.UnpauseGame();
            else 
                GameManager.Instance.PauseGame();
            FindObjectOfType<EscapeGUI>().Toggle();
        }

        if(Input.GetButtonDown("Fire1")) {
            animator.SetTrigger("fireTrigger");
        }

        if(Input.GetButtonDown("Quick1")) {
            this.inventory.ChooseSlot(0);
        }
        else if(Input.GetButtonDown("Quick2")) {
            this.inventory.ChooseSlot(1);
        }
        else if(Input.GetButtonDown("Quick3")) {
            this.inventory.ChooseSlot(2);
        }
    }

    // Public methods
    public PlayerInventory GetInventory() { return inventory; }
    public PlayerMovement GetMovement() { return movement; }
}
