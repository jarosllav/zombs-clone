﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    // Private fields
    [SerializeField] private Transform target;
    private Vector3 worldSize;
    private Vector2 margin = new Vector2(1, 1);

    // Unity methods
    private void FixedUpdate() {
        if(this.target != null) {
            this.lookAtTarget();
        }
    }

    // Private methods

    /// <summary>
    /// Center camera on target and clamps between world borders
    /// </summary>
    private void lookAtTarget() {
        Vector3 position = this.target.position;
        position.z = -10;

        if(Mathf.Abs(transform.position.x - position.x) > margin.x)
            position.x = Mathf.Lerp(transform.position.x, position.x, 3f * Time.deltaTime);
        if(Mathf.Abs(transform.position.y - position.y) > margin.y)
            position.y = Mathf.Lerp(transform.position.y, position.y, 3f * Time.deltaTime);

        var halfWidth = Camera.main.orthographicSize * ((float)Screen.width / Screen.height);

        position.x = Mathf.Clamp(position.x, halfWidth, worldSize.x - halfWidth);
        position.y = Mathf.Clamp(position.y, Camera.main.orthographicSize, worldSize.y - Camera.main.orthographicSize);

        transform.position = position;
    }

    // Public methods
    public void SetWorldSize(Vector3 size) { this.worldSize = size; }
}
