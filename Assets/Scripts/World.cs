﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class World : MonoBehaviour
{
    // Public fields
    [Header("Generator settings")]
    public Vector2 Size = new Vector2(50, 50);
    public TileBase tile01; // Background tile (for now its grass)
    public TileBase tile02; // Border tile (for now its ruled tile of trees)

    [Header("Resources prefabs")]
    public GameObject PrefabTree;
    public GameObject PrefabRock;

    // Private fields
    [Header("World generation settings")]
    [SerializeField] private int deathLimit = 4;                    // 
    [SerializeField] private int birthLimit = 5;                    // 
    [SerializeField] private int simulationSteps = 10;              // 
    [SerializeField] private float chanceToStartAlive = 0.45f;      // 
    [SerializeField] private float radius = 1.5f;                   // 
    [SerializeField] private int limit = 10;                        // 
    [SerializeField] private int border = 2;                        // 

    private WorldGenerator generator;
    private Tilemap[] layers;
    private bool[,] worldCells;

    // Unity methods
    private void Awake() {
        this.generator = new WorldGenerator();
    }

    // Public methods

    /// <summary>
    /// Generate whole world based on generation settings, place resources.
    /// NOTE: There must be two tilemaps in a scene. (First for background, second for borders and collider)
    /// </summary>
    public void Generate() { 
        #if UNITY_EDITOR
        this.generator = new WorldGenerator();
        #endif

        // Setup generator variables
        this.generator.Size = this.Size;
        this.generator.CellSize = 16;
        this.generator.DeathLimit = this.deathLimit;
        this.generator.BirthLimit = this.birthLimit;
        this.generator.SimulationSteps = this.simulationSteps;
        this.generator.ChanceToStartAlive = this.chanceToStartAlive;
        this.generator.Radius = this.radius;
        this.generator.Limit = this.limit;
        this.generator.Border = this.border;

        this.worldCells = this.generator.Generate();

        // Setup tilemaps
        Tilemap[] layers = FindObjectsOfType<Tilemap>();

        Vector3Int tilemapSize = new Vector3Int((int)this.Size.x, (int)this.Size.y, 0);
        for(int i = 0; i < layers.Length; ++i) {
            layers[i].size = tilemapSize;
            layers[i].ClearAllTiles();
        }

        for(int y = 0; y < this.Size.y; ++y) {
            for(int x = 0; x < this.Size.x; ++x) {
                Vector3Int tilePosition = new Vector3Int(x, y, 0);
                layers[0].SetTile(tilePosition, this.tile01);

                if(worldCells[x, y])
                    layers[1].SetTile(tilePosition, this.tile02);                    
            }
        }

        this.generator.PlaceResources(this.PrefabTree);
        this.generator.PlaceResources(this.PrefabRock);

        // Update world cells because generator placed resources
        this.worldCells = this.generator.GetMap();
    }

    /// <summary>
    /// Placing game object in a world at given position
    /// </summary>
    /// <param name="obj">Reference to the GameObject to be placed. (NOTE: Must be already instantiated)</param>
    /// <param name="position">Position on which it is to be placed.</param>
    /// <param name="size">Size of a GameObject in cells (Default value: Vector2Int(1, 1))</param>
    /// <returns>Is successful placed</returns>
    public bool Place(GameObject obj, Vector3 position, Vector2Int? size = null) { 
        Vector2Int objSize = size ?? Vector2Int.one;
        if(this.CanPlaceAtPosition(position, objSize)) {
            obj.transform.parent = GameObject.Find("_Dynamic").transform;
            this.SetArea(true, position, objSize);
            return true;
        }
        return false;
    }

    /// <summary>
    /// Sets grid area of cells to given boolean state. (Grid tells which cells are occupied by something)
    /// </summary>
    /// <param name="state">Boolean state (true means that area is occupied)</param>
    /// <param name="position">World position of area</param>
    /// <param name="size">Size of area</param>
    public void SetArea(bool state, Vector3 position, Vector2Int size) {
        for(int y = 0; y < size.y; ++y) {
            for(int x = 0; x < size.x; ++x) {
                int cellX = Mathf.FloorToInt(position.x) + x;
                int cellY = Mathf.FloorToInt(position.y) + y;
                this.worldCells[cellX, cellY] = state;
            }
        }
    }

    /// <summary>
    /// Check if something can be placed at area.
    /// </summary>
    /// <param name="position">World position</param>
    /// <param name="size">Size of area</param>
    /// <returns>Boolean value (true means that smth can be placed)</returns>
    public bool CanPlaceAtPosition(Vector3 position, Vector2Int size) {
        for(int y = 0; y < size.y; ++y) {
            for(int x = 0; x < size.x; ++x) {
                int cellX = Mathf.FloorToInt(position.x) + x;
                int cellY = Mathf.FloorToInt(position.y) + y;

                if(cellX > 0 && cellY > 0 && cellX < this.Size.x && cellY < this.Size.y) {
                    if(this.worldCells[cellX, cellY])
                        return false;
                }
                else 
                    return false;
            }
        }
        return true;
    }

    /// <summary>
    /// Check if given position is between world borders.
    /// </summary>
    /// <param name="position">World position</param>
    /// <returns>Boolean value (true means that position is between world borders)</returns>
    public bool IsIn(Vector3 position) {
        if(position.x < 0 || position.x >= this.Size.x || position.y < 0 || position.y >= this.Size.y)
            return false;
        return true;
    }

    public bool[,] GetMap() { return worldCells; }

    #if UNITY_EDITOR
    private void OnDrawGizmos() {
        if(worldCells != null) {
            for(int x = 0; x < worldCells.GetLength(0); ++x) {
                for(int y = 0; y < worldCells.GetLength(1); ++y) {  
                    if(worldCells[x, y])
                        Gizmos.color = Color.red;
                    else
                        Gizmos.color = Color.white;
                    Gizmos.DrawWireCube(new Vector3(x + 0.5f, y + 0.5f, 0), new Vector3(1, 1, 0.1f));
                }
            }
        }
    }
    #endif
}
