﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // Singleton
    private static GameManager instance;
    public static GameManager Instance { get { return instance; } }

    // Public fields
    public Vector2Int WaveStartTime;
    public int WaveTime = 45;
    public int DaysToStart = 3;

    // Private fields
    [SerializeField] private World world;
    [SerializeField] private Player player;
    [SerializeField] private FollowCamera camera;
    [SerializeField] private ZombieSpawner zombieSpawner;
    [SerializeField] private BuildingManager buildingManager;

    private GameObject baseObject;
    private int daysToStart;
    private bool isPaused = false;

    // Unity methods
    private void Awake() {
        if(instance != null && instance != this) 
            Destroy(this.gameObject);
        else 
            instance = this;
    }

    private void Start() {
        Building.onBuild += onBuild; 
        Building.onDestroy += onDestory; 

        this.generateGame();
    }

    // Private methods

    /// <summary>
    /// Event handler for placing building by player
    /// Checks if building is Base type and invoke ZombieSpawner to generate spawn points for enemies.
    /// </summary>
    private void onBuild(Building building) {
        BuildingType type = building.Type;

        if(type == BuildingType.BASE) {
            BuildingBase buildingBase = building as BuildingBase;
            this.baseObject = buildingBase.gameObject;

            this.zombieSpawner?.GeneratePoints(this.baseObject.transform.position, 10f);
        }
    }

    /// <summary>
    /// Event handler for destroying building
    /// If building was the base - game over.
    /// </summary>
    /// <param name="building"></param>
    private void onDestory(Building building) {
        if(building.gameObject == this.baseObject) {
            this.gameOver();
        }
    }

    /// <summary>
    /// Setup and display GameOver overlay i pause the game. 
    /// </summary>
    private void gameOver() {
        string info = "You survive: " + this.zombieSpawner.GetWaveCounter() + " waves";
        if(this.daysToStart <= 0) 
            info = "Build a base in " + this.DaysToStart + " days";

        GameOverGUI gameOverScript = FindObjectOfType<GameOverGUI>();
        if(gameOverScript) {
            gameOverScript.SetText(info);
            gameOverScript.Show();
        }

        this.PauseGame();
    }

    /// <summary>
    /// Invoke world generation and teleport player on random position 
    /// </summary>
    private void generateGame() {
        this.world.Generate();
        bool[,] map = this.world.GetMap();

        int attempts = 0;
        while(true) {
            int x = Random.Range(0, (int)this.world.Size.x);
            int y = Random.Range(0, (int)this.world.Size.y);

            if(x <= 0 || y <= 0)
                continue;

            if(!(map[x, y] && map[x - 1, y] && map[x, y - 1] && map[x - 1, y - 1])) {
                Vector3 playerPosition = new Vector3(x, y, 0);
                player.transform.position = playerPosition;
                break;
            }

            if(attempts >= 15) {
                player.transform.position = Vector3.zero;
                break;
            }

            ++attempts;
        }

        this.camera.transform.position = player.transform.position;
        this.camera.SetWorldSize(world.Size);

        this.daysToStart = this.DaysToStart;

        this.player.GetInventory().ChooseSlot(0);
    }

    // Public methods

    /// <summary>
    /// Event handler of game timer
    /// Starts new waves
    /// </summary>
    public void OnTime(int min, int sec) {
        int waveCounter = this.zombieSpawner.GetWaveCounter();
        if(/*min == WaveTime.x && */sec == this.WaveStartTime.y) {
            if(this.baseObject != null) {
                this.zombieSpawner?.StartWave(this.baseObject.transform);
            }
            else {
                this.daysToStart--;
                if(this.daysToStart <= 0) {
                    this.gameOver();
                }
            }
        }


        int waveEndMin = (this.WaveStartTime.y + this.WaveTime) / 60;
        int waveEndSec = this.WaveStartTime.y + this.WaveTime;
        if(sec == waveEndSec) {
            if(this.zombieSpawner.IsWaveStarted())
                this.zombieSpawner.StopWave();
            this.buildingManager.RegenerateBuildings();
        }
    }

    /// <summary>
    /// Restarts game
    /// </summary>
    public void RestartGame() {
        this.UnpauseGame();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Pausing game
    /// </summary>
    public void PauseGame() {
        this.isPaused = true;
        Time.timeScale = 0f;
    }

    /// <summary>
    /// Unpausing game
    /// </summary>
    public void UnpauseGame() {
        this.isPaused = false;
        Time.timeScale = 1f;
    }

    /// <summary>
    /// Closing game and exit to desktop
    /// </summary>
    public void ExitGame() {
        Application.Quit();
    }

    public bool IsGamePasued() { return this.isPaused; }

    public World GetWorld() { return world; }
    public Player GetPlayer() { return player; }
    public FollowCamera GetCamera() { return camera; }
    public ZombieSpawner GetZombieSpawner() { return zombieSpawner; }
    public BuildingManager GetBuildingManager() { return buildingManager; }
}
