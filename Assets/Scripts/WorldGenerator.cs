using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public enum TilemapLayers {
    Base, Collider, Resources
}

public class WorldGenerator
{
    // Public fields
    public Vector2 Size;
    public float CellSize;
    public int DeathLimit = 4;
    public int BirthLimit = 5;
    public int SimulationSteps = 10;
    public float ChanceToStartAlive = 0.45f;
    public float Radius = 1.5f;
    public int Limit = 10;
    public int Border = 2;

    // Private fields
    private PoissonDistribution poissonDistribution;
    private bool[,] cellMap;

    public WorldGenerator() {
        this.poissonDistribution = new PoissonDistribution();
    }

    // Private methods

    /// <summary>
    /// Make world border of given thickness. (To prevent to player go beyond map)
    /// </summary>
    /// <param name="thickness">Amount of cells</param>
    private void makeBorders(int thickness) {
        for(int y = 0; y < this.Size.y; ++y) {
            for(int x = 0; x < this.Size.x; ++x) {
                for(int i = 0; i < thickness; ++i) {
                    if(y == i || y == this.Size.y - i - 1 || x == i || x == this.Size.x - i - 1) {
                        this.cellMap[x, y] = true;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Processing one step of cellular automata simulation
    /// </summary>
    /// <returns>Cells map after simulation step</returns>
    private bool[,] processSimulation() {
        bool[,] map = new bool[(int)this.Size.x, (int)this.Size.y];

        for(int y = 0; y < this.Size.y; ++y) {
            for(int x = 0; x < this.Size.x; ++x) {
                int neigbours = this.countNeighbours(x, y);
                if(this.cellMap[x,y]) {
                    map[x, y] = neigbours >= this.DeathLimit;      
                }
                else {
                    map[x, y] = neigbours > this.BirthLimit;   
                }
            }
        }  

        return map;
    }

    /// <summary>
    /// Counts alive neighbour cells in map
    /// </summary>
    /// <param name="x">X axis map position</param>
    /// <param name="y">Y axis map position</param>
    /// <returns>Amount of alive neighbours</returns>
    private int countNeighbours(int x, int y) {
        int count = 0;
        for(int i = -1; i < 2; i++) {
            for(int j = -1; j < 2; j++) {
                int neighbourX = x + i;
                int neighbourY = y + j;

                if(i == 0 && j == 0)
                    continue;
                else if(neighbourX < 0 || neighbourY < 0 || neighbourX >= this.Size.x || neighbourY >= this.Size.y) {
                    ++count;
                }
                else if(this.cellMap[neighbourX, neighbourY]) {
                    ++count;
                }
            }
        }
        return count;
    }

    // Public methods
    
    /// <summary>
    /// Generate cell map, process simulation and remove unnecessary caverns (to prevent to player spawn inside small area withour resources)
    /// </summary>
    /// <returns>Final cell map</returns>
    public bool[,] Generate() {
        this.cellMap = new bool[(int)this.Size.x + 1, (int)this.Size.y + 1];

        // Initilize map
        for(int y = 0; y < this.Size.y; ++y) {
            for(int x = 0; x < this.Size.x; ++x) {
                bool isAlive = Random.Range(0f, 1f) < this.ChanceToStartAlive;
                this.cellMap[x, y] = isAlive;
            }
        }       

        // Process simulation steps
        for(int i = 0; i < SimulationSteps; ++i) {
            this.cellMap = processSimulation();
        }

        // Flood fill
        List<List<Vector2Int>> caverns = new List<List<Vector2Int>>();
        bool[,] cavernMap = (bool[,])this.cellMap.Clone();
        for(int y = 0; y < this.Size.y; ++y) {
            for(int x = 0; x < this.Size.x; ++x) {
                if(cavernMap[x, y])
                    continue;

                List<Vector2Int> cavern = new List<Vector2Int>();
                List<Vector2Int> total = new List<Vector2Int>();
                cavern.Add(new Vector2Int(x, y));
                total.Add(new Vector2Int(x, y));

                while(cavern.Count > 0) {
                    Vector2Int position = cavern[0];
                    cavern.RemoveAt(0);
                    if(!cavernMap[position.x, position.y]) {
                        cavernMap[position.x, position.y] = true;

                        for(int i = -1; i < 2; i++) {
                            for(int j = -1; j < 2; j++) {
                                int neighbourX = position.x + i;
                                int neighbourY = position.y + j;

                                if(i == 0 && j == 0)
                                    continue;
                                else if(neighbourX < 0 || neighbourY < 0 || neighbourX >= this.Size.x || neighbourY >= this.Size.y)
                                    continue;
                                else if(!cavernMap[neighbourX, neighbourY]) {
                                    cavern.Add(new Vector2Int(neighbourX, neighbourY));
                                    total.Add(new Vector2Int(neighbourX, neighbourY));
                                }
                            }
                        }
                    }
                } 
                caverns.Add(total);
            }
        } 

        // Choose the biggest cavern
        int mainCavern = 0;
        for(int i = 0; i < caverns.Count; ++i) {
            if(caverns[mainCavern].Count < caverns[i].Count) {
                mainCavern = i;
            }
        }

        // Remove any other caverns which its not the main chamber
        for(int i = 0; i < caverns.Count; ++i) {
            if(i != mainCavern) {
                for(int j = 0; j < caverns[i].Count; ++j) {
                    Vector2Int position = caverns[i][j];
                    this.cellMap[position.x, position.y] = true;
                }
            }
        }

        // Call to make borders
        this.makeBorders(this.Border);

        return this.cellMap;
    }

    /// <summary>
    /// Spread resources over cell map
    /// </summary>
    /// <param name="prefab">Resource prefab</param>
    public void PlaceResources(GameObject prefab) {
        this.poissonDistribution.Limit = Limit;
        this.poissonDistribution.Radius = Radius;
        this.poissonDistribution.Size = Size;

        Vector3[] points = this.poissonDistribution.Generate();
        Transform holder = GameObject.Find("_Dynamic").transform;

        for(int i = 0; i < points.Length; ++i) {
            int x = Mathf.FloorToInt(points[i].x);
            int y = Mathf.FloorToInt(points[i].y);
            if(this.cellMap[x, y] || this.cellMap[x - 1, y] || this.cellMap[x, y - 1] || this.cellMap[x - 1, y - 1])
                continue;

            Vector3 position = new Vector3(x, y, 0);
            GameObject obj = GameObject.Instantiate(prefab, position, Quaternion.identity);
            obj.transform.parent = holder;

            this.cellMap[x, y] = true;
            this.cellMap[x - 1, y] = true;
            this.cellMap[x, y - 1] = true;
            this.cellMap[x - 1, y - 1] = true;
        }
    }

    public bool[,] GetMap() { return cellMap; }
}
