﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSpawner : MonoBehaviour
{
    // Public fields
    public GameObject[] Zombies;

    // Private fields
    private List<Vector3> spawnPositions;
    private List<GameObject> spawnedZombies;
    private int waveCounter = 0;
    private bool waveStarted = false;

    // Unity methods
    private void Awake() {
        // ...
    }

    // Private methods

    /// <summary>
    /// Activate enemies Game Objects with a little delay between each other.
    /// </summary>
    IEnumerator ActivateEnemies() {
        foreach(GameObject zombie in this.spawnedZombies) {
            zombie.SetActive(true);
            yield return new WaitForSeconds(0.1f);
        }
    }

    /// <summary>
    /// Event handler for zombie death event, removes zombie form list and checks if that was a last one (if it was - call StopWave())
    /// </summary>
    /// <param name="zombie">Reference to GameObject of zombie</param>
    private void onZombieDead(GameObject zombie) {
        this.spawnedZombies.Remove(zombie);

        if(this.spawnedZombies.Count <= 0) {
            this.StopWave();
        }
    }

    // Public methods

    /// <summary>
    /// Generate spawn points for enemy based on position
    /// NOTE: Amount of spawn points is hardcoded for now.
    /// </summary>
    /// <param name="center">World position around which it will generate points</param>
    /// <param name="radius">Distance between points and center position</param>
    public void GeneratePoints(Vector3 center, float radius) {
        this.spawnPositions = new List<Vector3>();
        int spotsCount = 8;

        //TODO: This have to be rewrite
        for(int i = 0; i < spotsCount; ++i) {
            float angle = 360 - (360 / spotsCount) * i;
            float x = Mathf.Cos(angle * Mathf.Deg2Rad) * radius + center.x;
            float y = Mathf.Sin(angle * Mathf.Deg2Rad) * radius + center.y;
            Vector3 position = new Vector3(x, y, 0);

            if(GameManager.Instance.GetWorld().IsIn(position)) {
                this.spawnPositions.Add(position);

                GameObject obj = new GameObject("SpawnSpot" + i);
                obj.transform.position = new Vector3(x, y, 0);
            }
        }
    }

    /// <summary>
    /// Starts new wave - calculate amount of enemies and spawn them.
    /// </summary>
    /// <param name="target">Enemies target</param>
    public void StartWave(Transform target) {
        if(waveCounter % 10 == 0) {
            //todo Special wave, add some boss or something?
        }

        int enemiesCount = (int)Mathf.Pow(waveCounter, 2) - waveCounter + 3;

        Transform _dynamic = GameObject.Find("_Dynamic").transform;

        spawnedZombies = new List<GameObject>();
        for(int i = 0; i < enemiesCount; ++i) {
            Vector3 position = spawnPositions[Random.Range(0, spawnPositions.Count)];
            GameObject prefab = Zombies[Random.Range(0, Zombies.Length)];

            GameObject zombie = Instantiate(prefab, position, Quaternion.identity);
            zombie.transform.parent = _dynamic;
            zombie.SetActive(false);

            zombie.GetComponent<Zombie>().Target = target;
            zombie.GetComponent<Zombie>().OnDead.AddListener(this.onZombieDead);
            spawnedZombies.Add(zombie);
        }

        StartCoroutine(ActivateEnemies());

        waveCounter++;
        waveStarted = true;
    }

    /// <summary>
    /// Stopping wave and destroy all spawned zombies.
    /// </summary>
    public void StopWave() {
        waveStarted = false;

        foreach(GameObject zombie in spawnedZombies) {
            Destroy(zombie);
        }
        spawnedZombies.Clear();
    }

    public int GetWaveCounter() { return this.waveCounter; }
    public bool IsWaveStarted() { return this.waveStarted; }
}
