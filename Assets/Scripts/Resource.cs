﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ResourcesTypes {Wood, Rock, Gold, Special}

public class Resource : MonoBehaviour
{
    // Public fields
    public ResourcesTypes Type;
    
    // Private fields
    private float shakeSpeed = 0.5f;
    private float shakeForce = 10f;
    private float shakeTime = 0.3f;
    private float shakeTimer = 1.0f;

    private Vector3 basePosition;
    private bool isShaking = false;

    // Unity methods
    private void Awake() {
        this.basePosition = transform.position;
    }

    private void Update() {
        if(this.isShaking) {
            this.processShakeAnimation();
        }
    }

    // Private methods
    private void processShakeAnimation() {
        this.shakeTimer -= Time.deltaTime;
        float shake = this.shakeSpeed * Time.deltaTime;

        if(shakeTimer <= 0) {
            transform.position = this.basePosition;
            this.isShaking = false;
        }
        else {
            transform.position = Vector3.MoveTowards(transform.position, this.basePosition + Random.insideUnitSphere, shake);
        }
    }

    // Public methods
    public void Shake() {
        this.isShaking = true;
        this.shakeTimer = shakeTime;
    }
}
