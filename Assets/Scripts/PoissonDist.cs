﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoissonDistribution
{
    public float Radius;
    public int Limit;
    public Vector2 Size;

    private Vector2[,] grid;
    
    public Vector3[] Generate() {
        List<Vector3> points = new List<Vector3>();
        List<Vector2> active = new List<Vector2>();

        float cellSize = Mathf.Floor(Radius / Mathf.Sqrt(2));
        int width = Mathf.CeilToInt(Size.x / cellSize) + 1;
        int height = Mathf.CeilToInt(Size.y / cellSize) + 1;
    
        grid = new Vector2[width, height];

        Vector2 point0 = new Vector2(Random.Range(0, Size.x), Random.Range(0, Size.y));

        for(int x = 0; x < width; ++x) {
            for(int y = 0; y < height; ++y) {
                grid[x, y] = Vector2.zero;
            }
        }

        insertPoint(cellSize, point0);
        points.Add(point0);
        active.Add(point0);

        while(active.Count > 0) {
            int id = Random.Range(0, active.Count);
            Vector2 point = active[id];

            bool found = false;
            for(int i = 0; i < Limit; ++i) {
                float theta = Random.Range(0, 360);
                float radius = Random.Range(Radius, 2 * Radius);
                float px = point.x + radius * Mathf.Cos(Mathf.Deg2Rad * theta);
                float py = point.y + radius * Mathf.Sin(Mathf.Deg2Rad * theta);

                Vector2 newPoint = new Vector2(px, py);
                
                if(!isValidPoint(cellSize, newPoint))
                    continue;

                points.Add(newPoint);
                active.Add(newPoint);
                insertPoint(cellSize, newPoint);
                found = true;
                break;
            }

            if(!found)
                active.RemoveAt(id);
        }

        return points.ToArray();
    }

    private void insertPoint(float cellSize, Vector2 point) {
        int x = Mathf.FloorToInt(point.x / cellSize);
        int y = Mathf.FloorToInt(point.y / cellSize);
        grid[x, y] = point;
    }

    private bool isValidPoint(float cellsize, Vector2 point) {
        if(point.x < 0 || point.y < 0 || point.x > Size.x || point.y > Size.y)
            return false;

        int xindex = Mathf.FloorToInt(point.x / cellsize);
        int yindex = Mathf.FloorToInt(point.y / cellsize);
        int i0 = Mathf.Max(xindex - 1, 0);
        int i1 = Mathf.Min(xindex + 1, grid.GetLength(0) - 1);
        int j0 = Mathf.Max(yindex - 1, 0);
        int j1 = Mathf.Min(yindex + 1, grid.GetLength(1) - 1);

        for (int i = i0; i <= i1; i++) {
            for (int j = j0; j <= j1; j++) {
                if (grid[i, j] != null) {
                    if(Vector2.Distance(grid[i, j], point) < Radius) {
                        return false;
                    }
                }
            }
        }

        return true;
    }
}
