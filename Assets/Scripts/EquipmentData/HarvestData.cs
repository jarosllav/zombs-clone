﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Harvest data", menuName = "Equipment data/Harvest", order = 2)]
public class HarvestData : EquipmentData
{
    public int CollectSize;
}
