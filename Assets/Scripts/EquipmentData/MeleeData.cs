﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Melee data", menuName = "Equipment data/Melee", order = 3)]
public class MeleeData : EquipmentData
{
    public int Damage;
}
