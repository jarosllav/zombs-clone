﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum EquipmentType {HARVEST, MELEE, RANGED}

[CreateAssetMenu(fileName = "Equipment data", menuName = "Equipment data/Basic", order = 1)]
public class EquipmentData : ScriptableObject
{
    public Sprite Texture;
    public int Quality;
}
