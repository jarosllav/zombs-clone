﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Zombie : MonoBehaviour
{
    // Events
    public UnityEvent<GameObject> OnDead = new UnityEvent<GameObject>();
    
    // Public fields
    public Transform Target;
    public float AttackRange = 1f;
    public float AttackSpeed = 1f;
    public float Speed = 2f;
    public int Health = 10;

    // Private fields
    [SerializeField] private Slider healthUI;
    private Transform lastTarget;
    private float lastHit = 0f;

    // Unity methods
    private void Awake() {
        this.healthUI.maxValue = Health;
        this.healthUI.value = Health;
        this.healthUI.gameObject.SetActive(false);
    }

    private void Update() {
        if(this.Target) {
            this.moveToTarget();
        }
    }

    // Private methods
    
    /// <summary>
    /// Move zombie toward target (Player), if it hits obstacle zombie will try to get around the obstacle.
    /// </summary>
    private void moveToTarget() {
        Vector2 movement = Vector2.MoveTowards(transform.position, this.Target.position, this.Speed * Time.deltaTime);

        Vector3 direction = this.Target.position - transform.position;
        direction.Normalize();
        float angle = Mathf.Rad2Deg * Mathf.Atan2(direction.y, direction.x);
        transform.rotation = Quaternion.Euler(0f, 0f, angle - 90);

        bool movingAround = false;

        if(this.lastHit > 0) {
            this.lastHit -= Time.deltaTime;
        }
        else {
            gameObject.layer = 2;
            RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, 5f);
            gameObject.layer = 0;

            if(hit.collider != null && hit.collider.tag != "Zombie") {
                if(hit.distance <= this.AttackRange) {
                    if(hit.transform.tag == "Player") {
                        this.lastHit = this.AttackSpeed;
                    }
                    else if(hit.transform.tag == "Building") {
                        Building building = hit.transform.GetComponent<Building>();
                        building.Hit();
                        this.lastHit = this.AttackSpeed;
                    }
                    else if(hit.transform.tag == "Resource") {
                        Vector3 around = Vector2.Perpendicular(direction);
                        Vector2 moveAround = Vector2.MoveTowards(transform.position, around * 100, this.Speed * Time.deltaTime);
                        transform.position = moveAround;
                        movingAround = true;
                    }
                }
            }
        }
        
        if(Vector2.Distance(movement, this.Target.position) > this.AttackRange && !movingAround)
            transform.position = movement;
    }

    // Public methods

    /// <summary>
    /// Deal damage to zombie
    /// </summary>
    /// <param name="strength">Strength of hit</param>
    public void Hit(int strength) {
        this.Health -= strength;

        this.healthUI.value = this.Health;
        if(!this.healthUI.IsActive())
            this.healthUI.gameObject.SetActive(true);
            
        if(this.Health <= 0) {
            this.OnDead.Invoke(gameObject);
            Destroy(gameObject);
        }
    }
}
