using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "Building data", menuName = "Building data/Basic", order = 1)]
public class BuildingData : ScriptableObject
{
    public Sprite Texture;
    public Vector2Int Size;
    public int Limit = 0;
    public string Name;
    public int Wood;
    public int Rock;
    public int Health = 10;

    public bool IsRanged = false;
    public GameObject Ammunition;
    public float Radius = 10f;
    public float AttackSpeed = 1f;
    public int Damage = 1;
}


#if UNITY_EDITOR
[CustomEditor(typeof(BuildingData), true)] 
public class BuildingDataEditor : Editor {
    override public void OnInspectorGUI() {
        BuildingData data = target as BuildingData;

        EditorGUILayout.LabelField("Default fields", EditorStyles.boldLabel);
        EditorGUILayout.Space();

        data.Texture = (Sprite)EditorGUILayout.ObjectField("Texture", data.Texture, typeof(Sprite), allowSceneObjects: true);
        data.Size = EditorGUILayout.Vector2IntField("Size", data.Size);
        data.Limit = EditorGUILayout.IntField("Limit of buildings", data.Limit);
        data.Name = EditorGUILayout.TextField("Name", data.Name);
        
        EditorGUILayout.Space();
        data.Wood = EditorGUILayout.IntField("Wood cost", data.Wood);
        data.Rock = EditorGUILayout.IntField("Rock cost", data.Rock);

        EditorGUILayout.Space();
        data.Health = EditorGUILayout.IntField("Health", data.Health);

        EditorGUILayout.LabelField("Ranged fields", EditorStyles.boldLabel);
        EditorGUILayout.Space();

        data.IsRanged = EditorGUILayout.Toggle("IsRanged", data.IsRanged);
        if(data.IsRanged) {
            data.Ammunition = (GameObject)EditorGUILayout.ObjectField("Ammo prefab", data.Ammunition, typeof(GameObject), allowSceneObjects: true);
            data.Radius = EditorGUILayout.FloatField("Radius", data.Radius);
            data.AttackSpeed = EditorGUILayout.FloatField("Attack speed", data.AttackSpeed);
            data.Damage = EditorGUILayout.IntField("Damage", data.Damage);
        }
    }
}
#endif